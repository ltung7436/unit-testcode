package org.example.test;
import org.junit.jupiter.api.Test;

import static org.testng.AssertJUnit.assertEquals;

public class MathUtilTest {
    MathUtil util = new MathUtil();

    //Chúng ta viết vài test case nhẹ nhàng, thông qua Annotation @Test và hàm assertEquals() nhé!!!!

    @Test
    void Test1() {
        assertEquals(util.sum(1, 1), 2);
    }

    @Test
    void Test2() {
        assertEquals(util.sum(1, 1), 10);
    }
    @Test
    void Test3() {
        assertEquals(util.sum(1, 1), 6);
    }
}
